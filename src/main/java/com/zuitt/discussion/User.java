package com.zuitt.discussion;

import org.springframework.web.bind.annotation.*;

public class User {
    // Properties
    private String name;

    //Constructors
    public User(){};
    public User (String name){
        this.name = name;
    }

    // Getters
    public String getName(){
        return name;
    }
}
