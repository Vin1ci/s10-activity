package com.zuitt.discussion;

import org.springframework.web.bind.annotation.*;

public class Post {

    // Properties
    private String title;

    //Constructors
    public Post(){};
    public Post (String title, String content){
        this.title = title;
    }

    // Getters
    public String getTitle(){
        return title;
    }
}
